package jid.budgetlogger;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final InitFragment initFragment = new InitFragment();
        Button btnGoToInit = (Button)findViewById(R.id.btnGoToInitFragment);
        btnGoToInit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = getSupportFragmentManager();
                initFragment.show(fm, "fragment_init");
            }
        });

        DictionaryOpenHelper dictionaryOpenHelper = new DictionaryOpenHelper(this);
        String databaseName = dictionaryOpenHelper.getDatabaseName();
        Log.d("something", "dictionary?: " + databaseName);
    }
}
