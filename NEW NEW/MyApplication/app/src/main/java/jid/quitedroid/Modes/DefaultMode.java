package jid.quitedroid.Modes;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import jid.quitedroid.Activity.MainActivity;
import jid.quitedroid.R;

/**
 * Created by Dawin on 4/14/2016.
 */
public class DefaultMode extends Mode implements IMode{
    public static final int icon = R.drawable.smallnormalmodeicon;
    public static final String name = "Default Mode";
    public static final String soundType = "normal";
    public static final int COLOR = Color.rgb(0,202,3);

    public DefaultMode(Context context, String eventTitle){
        super(context, icon, name, soundType,eventTitle, COLOR);
    }

    @Override
    public Notification getMyActivityNotification(Context context, String mode, int icon){
        PendingIntent startActivity = PendingIntent.getActivity(
                context,
                0,
                new Intent(context, MainActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle("QuietDroid")
                .setContentText(mode)
                .setSmallIcon(icon)
                .setContentIntent(startActivity)
                .setOngoing(true)
                .setAutoCancel(false);

        return builder.build();

    }

    /*public static boolean isModeKeyword(String keyword) {
        //for normal mode
        if(keywords.size() <= 0) return false;
        keyword = keyword.toLowerCase();
        for (String s : keywords)
            if (keyword.contains(s))
                return true;
        return false;
    }*/
}
