package jid.quitedroid.Interface;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.PointTarget;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import java.util.ArrayList;
import java.util.List;

import jid.quitedroid.GlobalConstants;
import jid.quitedroid.Handlers.ContextHandler;
import jid.quitedroid.R;

/**
 * This class allows you to have multiple showcases on one screen, you can then page through
 * each one as you press the 'ok' button
 */
public class ShowcaseViewsForceModeDialog {

    private final Activity activity;
    public int index = 0;

    SharedPreferences sharedPreferences;

    /**
     * @param activity               The activity containing the views you wish to showcase
     */
    public ShowcaseViewsForceModeDialog(Activity activity) {
        this.activity = activity;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ContextHandler.getContext()); //activity.getApplicationContext()
    }


    public void setChainClickListener(final ShowcaseView viewTemplate) {
        viewTemplate.overrideButtonClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index++;
                viewTemplate.hide();
                show();
            }
        });
    }


    private ShowcaseView GetShowCaseView(int x, int y, String message, boolean blockAllTouches){
        ShowcaseView.Builder show = new ShowcaseView.Builder(activity, true)
                .withNewStyleShowcase()
                .setTarget(new PointTarget(x,y))
                .setStyle(ShowcaseView.ABOVE_SHOWCASE)
                .setContentText(message);
        if(blockAllTouches)
            show.blockAllTouches();
        else
            show.doNotBlockTouches();
        ShowcaseView showCase = show.build();
        setChainClickListener(showCase);
        return showCase;
    }
    /**
     * Showcases will be shown in the order they where added, continuing when the button is pressed
     */
    public void show() {
        //Took me ages to figure out that you need to dynamically make a whole new showcase in order for it to work, what a weird issue
        //it's only open source so it isn't perfect, guess we have to manage with what we have
        switch (index){
            case 0:
                int x = activity.findViewById(R.id.tabHost).getWidth() / 2;
                int y = activity.findViewById(R.id.tabHost).getHeight() / 2;
                GetShowCaseView(
                        x,
                        y,
                        "\nModes can be forced for this specific event" +
                                "\nThis can be reversed later",
                        false);

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(GlobalConstants.FIRST_BOOT_FORCE_DIALOG, false);
                editor.apply();
                break;
            case 3:
                break;

        }
    }

}
