package jid.quitedroid.Features;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import jid.quitedroid.Handlers.ContextHandler;
import jid.quitedroid.Database.DatabaseOperations;
import jid.quitedroid.Database.TableData;
import jid.quitedroid.GlobalConstants;
import jid.quitedroid.Activity.MainActivity;
import jid.quitedroid.Modes.SilentMode;
import jid.quitedroid.Modes.VibrateMode;
import jid.quitedroid.Modes.Mode;
import jid.quitedroid.Modes.DefaultMode;
import jid.quitedroid.R;
import jid.quitedroid.Receivers.CalendarProcessor;
import jid.quitedroid.Interface.ShowcaseViewsForceModeDialog;

/**
 * Created by JiYeon on 2016-08-11.
 */

public class ForceModeHandler {
    public static final int
            DEFAULT_MODE = 0,
            VIBRATE_MODE = 1,
            SILENT_MODE = 2,
            REVERT = 3;

    private DatabaseOperations databaseOperations = new DatabaseOperations(ContextHandler.getContext());
    private List<String> eventIds, eventModes, eventTitles, eventStartTimes, eventRRULE;
    private SharedPreferences sharedQuiteDroidEnabled;
    private int mode;
    ListView lv;

    public ForceModeHandler(final MainActivity mainActivity){
        lv = (ListView) mainActivity.findViewById(R.id.quickAddHandlerView);
    }

    public void displayNormalModeEvents(final MainActivity mainActivity) {
        //Exit if quite droid is note enabled
        sharedQuiteDroidEnabled = PreferenceManager.getDefaultSharedPreferences(mainActivity);
        boolean quiteDroid_isEnabled = sharedQuiteDroidEnabled.getBoolean(GlobalConstants.QUITEDROID_ENABLED, false);
        if(!quiteDroid_isEnabled){
            return;
        }

        //Layout/ refresh UI on the event tab
        refreshEventTabUI(mainActivity);
    }

    private void refreshEventTabUI(final MainActivity mainActivity){
        //Process week
        CalendarProcessor.processWeek(ContextHandler.getContext());

        //set hidden data fields
        setHiddenIDField();

        //If there are no events, display that there are no events to user
        TextView textViewNoCalls = (TextView)mainActivity.findViewById(R.id.noEventsText);

        //If no events have been fetched display to user that there are no upcoming events
        try{
            if(!eventIds.isEmpty()){
                textViewNoCalls.setVisibility(View.INVISIBLE);
            }else{
                textViewNoCalls.setVisibility(View.VISIBLE);
            }
        }catch(Exception ex){
            //If an exception has been thrown, there are no fetched events
            textViewNoCalls.setVisibility(View.VISIBLE);
        }

        //Set hidden data fields: event's title, start time, mode and RRULE
        setHiddenTitleField();
        setHiddenTimeField();
        setHiddenModeField();
        setHiddenRRULEField();

        //Edit the user interface for Quick Add Handler (Events Tab)
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(mainActivity, android.R.layout.simple_list_item_1 , databaseOperations.getAllEventTitles()) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater mInflater = (LayoutInflater)
                        getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = mInflater.inflate(android.R.layout.simple_list_item_1, parent, false);

                //Set a coloured row according to the mode colour
                try {
                    view.setBackground(getColoredRowDrawable(Integer.parseInt(eventModes.get(position))));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //If this event has a forced mode (Exists in the forced table), display the text [FORCED] after the event's title
                if(databaseOperations.isEventInForcedDb(eventTitles.get(position), eventIds.get(position)) != -1){
                    //Set Title and Event Time
                    TextView textView = (TextView) view;
                    textView.setTextColor(Color.BLACK); //equivalent to dark gray or #939393
                    textView.setText(Html.fromHtml("<b>" + eventTitles.get(position) + " [FORCED]" + "</b>" +  "<br />" +
                            "<small><i>" + eventStartTimes.get(position) + "</i></small>"));
                } else {
                    //If this event does not have a forced mode, display the text event's title and start time
                    TextView textView = (TextView) view;
                    textView.setTextColor(Color.BLACK); //equivalent to dark gray or #939393
                    textView.setText(Html.fromHtml("<b>" + eventTitles.get(position) + "</b>" +  "<br />" +
                            "<small><i>" + eventStartTimes.get(position) + "</i></small>"));
                }

                return view;
            }
        };


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position,
                                    long id) {
                //When user clicks on an event that has been displayed on the events tab
                //Display the new pop up screen with options that allow users to force change the mode
                displayToggleModeFragment(adapterView, position, eventIds.get(position), mainActivity, view);
            }

        });

        //Set adapter
        lv.setAdapter(adapter);
    }
    
    private Drawable getColoredRowDrawable(int color) throws Exception {
        android.content.res.Resources res = ContextHandler.getContext().getResources();
        switch(color){
            case DEFAULT_MODE:
                return res.getDrawable(R.drawable.rounded_rectangle_green);
            case VIBRATE_MODE:
                return res.getDrawable(R.drawable.rounded_rectangle_orange);
            case SILENT_MODE:
                return res.getDrawable(R.drawable.rounded_rectangle_red);
            default:
                throw new Exception();
        }
    }

    //Display pop up screen allowing user to force change a mode of an event
    private void displayToggleModeFragment(AdapterView newAdapterView, final int position, final String hiddenEventID, final MainActivity mainActivity, final View view) {
        //Tutorial that is shown when the user first clicks on an event
        if(PreferenceManager.getDefaultSharedPreferences(mainActivity).getBoolean(GlobalConstants.FIRST_BOOT_FORCE_DIALOG, true)){
            ShowcaseViewsForceModeDialog show = new ShowcaseViewsForceModeDialog(mainActivity);
            show.show();
        }

        //Include the items (options such as DEFAULT MODE, VIBRATE MODE and SILENT MODE)
        List<CharSequence> items = new ArrayList<>();
        items.add(Html.fromHtml(" <font color=" + Mode.getModeColor(DEFAULT_MODE) + ">" + DefaultMode.name + "</font> "));
        items.add(Html.fromHtml(" <font color=" + Mode.getModeColor(VIBRATE_MODE) +  ">" + VibrateMode.name + "</font> "));
        items.add(Html.fromHtml(" <font color=" + Mode.getModeColor(SILENT_MODE) + ">" + SilentMode.name + "</font> "));

        //If event has a forced mode, display new option "Revert to keyword detection"
        if(databaseOperations.isEventInForcedDb(eventTitles.get(position), eventIds.get(position)) != -1){
            items.add(Html.fromHtml(" <font color=\"black\">Revert to keyword detection</font> "));
        }

        CharSequence[] arrItems = items.toArray(new CharSequence[items.size()]);

        final AdapterView adapterView = newAdapterView;
        final int current_event_mode = Integer.parseInt(eventModes.get(position));

        AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
        builder.setTitle("Change mode of event...");

        //Build radio button to allow user to choose an option
        builder.setSingleChoiceItems(arrItems, current_event_mode, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                mode = item;
            }
        });

        // Set up the buttons
        builder.setPositiveButton("Change Mode", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Revert on click ("Revert to keyword detection")
                if(mode == REVERT){
                    //add a delete from database function, and remove the forced entry
                    databaseOperations.deleteEventInForcedDb(eventTitles.get(position), eventIds.get(position));
                    refreshEventTabUI(mainActivity);

                }else if (current_event_mode != mode) {
                    try {
                        /**
                         * If user changed the event to a new mode, reprocess calendar *
                         * */

                        //Change mode of event in RAW database table
                        databaseOperations.setNewEventToggleMode(hiddenEventID, mode);
                        //Change color of list view item to GREEN
                        adapterView.getChildAt(position).setBackgroundColor(Mode.getModeColor(mode));

                        //Change text to include [FORCED]
                        //Set Title and Event Time
                        TextView textView = (TextView) view;
                        textView.setTextColor(Color.BLACK); //equivalent to dark gray or #939393
                        textView.setText(Html.fromHtml("<b>" + eventTitles.get(position) + " [FORCED]" + "</b>" + "<br />" +
                                "<small><i>" + eventStartTimes.get(position) + "</i></small>"));

                        //Change hidden event mode field to the new mode
                        eventModes.set(position, Integer.toString(mode));
                        //Transfer event from raw db to forced db
                        databaseOperations.transferEventFromRawToForced(eventTitles.get(position), eventIds.get(position));
                    }catch(Exception e){
                        //If there is an exception, refresh the tab user interface
                        refreshEventTabUI(mainActivity);
                        Toast.makeText(ContextHandler.getContext(), "Event doesn't exist, refreshing...", Toast.LENGTH_LONG).show();
                    }
                }
                //refresh the database by reprocessing to update the UI
                CalendarProcessor.processWeek(ContextHandler.getContext());
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //When user clicks on cancel, close the pop up dialog
                dialog.cancel();
            }
        });

        builder.show();

        //Check if it is an recurring event, if it is display a warning message to the user
        checkIfRecurringEvent(position, mainActivity);

    }

    //Check if the selected event is a recurring event, if it is a recurring event show a warning mesage
    private void checkIfRecurringEvent(int position, MainActivity mainActivity){
        if(!eventRRULE.get(position).equals("Normal Event")){
            displayHelpMessage(mainActivity, GlobalConstants.QUICK_ADD_WARNING_MSG);
        }
    }

    //Display a help message to the user on a new pop up screen
    private void displayHelpMessage(MainActivity mainActivity, String helpMessage){
        AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
        builder.setTitle("Information");
        builder.setMessage(helpMessage);
        builder.setIcon(android.R.drawable.ic_dialog_info);

        // Set up the buttons
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }

    //Set hidden title field
    private void setHiddenTitleField(){
        eventTitles = databaseOperations.getAllEventTitles();
    }

    //Set hidden ID field
    private void setHiddenIDField() {
        eventIds = databaseOperations.getAllEventIds(TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS);
    }

    //Set hidden start time field
    private void setHiddenTimeField(){
        eventStartTimes = databaseOperations.getAllEventTimes();
    }

    //Set hidden mode field
    private void setHiddenModeField(){
        eventModes = databaseOperations.getAllEventModes(TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS);
    }

    //Set hidden RRULE field
    private void setHiddenRRULEField(){
        eventRRULE = databaseOperations.getAllEventRRULE();
    }

    //Set on touch listener to the events list
    public void setOnTouchListener(View.OnTouchListener listener) {
        lv.setOnTouchListener(listener);
    }
}
