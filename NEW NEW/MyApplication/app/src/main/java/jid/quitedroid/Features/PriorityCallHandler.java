package jid.quitedroid.Features;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.Calendar;
import java.util.Map;

import jid.quitedroid.GlobalConstants;
import jid.quitedroid.Receivers.SetFeatureReceiver;


/**
 * Created by JiYeon on 2016-07-25.
 */

//This class controls the functionality of the priority caller feature
//If the same caller calls more than three times within the same event (set to silent mode), do not block the caller for the rest of the day
//Only supported for android versions 4.4 - 5.0
public class PriorityCallHandler {
    private static final String PriorityCallerList = "PriorityCallerList";
    private static final String ExceptionList = "ExceptionList" ;
    private SharedPreferences priorityCallerSharedPreferences, exceptionListSharedPreferences, defaultSharedPreferences;
    private Context context;
    private AlarmManager am;

    public PriorityCallHandler(Context context){
        this.context = context;
        priorityCallerSharedPreferences = context.getSharedPreferences(PriorityCallerList, Context.MODE_PRIVATE);
        exceptionListSharedPreferences = context.getSharedPreferences(ExceptionList, Context.MODE_PRIVATE);
        defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        //Set repeating alarm daily to reset priority caller list and exception list
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 1); // For 1 AM or 2 AM

        //Intent
        PendingIntent pi = PendingIntent.getBroadcast(context, 0,
                new Intent(context, SetFeatureReceiver.class), 0);

        //Initialize Alarm Manager
        am = (AlarmManager)context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        am.setInexactRepeating(AlarmManager.RTC, calendar.getTimeInMillis() , AlarmManager.INTERVAL_DAY, pi);
    }

    public Boolean getEnabled(){
        return defaultSharedPreferences.getBoolean(GlobalConstants.PRIORITYCALLER_ENABLED, false);
    }

    //When a call is blocked, record the caller's number in shared preferences
    public void recordCaller(String callerNumber){
        //If QuiteDroid has not been enabled, do not continue
        if(!getEnabled()){return;}
        String blockedCallerNumber = callerNumber;

        //Find out how many times the same caller called
        int freq = priorityCallerSharedPreferences.getInt(blockedCallerNumber, 0);

        //If the same caller called for more than three times, and has not been included to the exception list
        //Include the caller into the exception name
        if(freq > 2 && freq != 999){
            SharedPreferences.Editor exceptionListEditor = exceptionListSharedPreferences.edit();
            exceptionListEditor.putString(getContactName(context, callerNumber), callerNumber);
            exceptionListEditor.commit();
            //Set frequency to 999 (Easier to delete later)
            freq = 998;
        }

        SharedPreferences.Editor editor = priorityCallerSharedPreferences.edit();
        //Increment frequency by one and set as new frequency
        editor.putInt(blockedCallerNumber, freq + 1);

        //Commit
        editor.commit();
    }

    //Get the caller's name from the system's caller list using their phone number
    private static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return "Priority Caller";
        }
        String contactName = null;
        if(cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if(cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return contactName;
    }

    //Reset all the callers in the priority caller list unless frequency is equal to 999
    public void resetCallerList(){
        if(!getEnabled()){return;}

        SharedPreferences.Editor editor = priorityCallerSharedPreferences.edit();
        Map<String,?> keys = priorityCallerSharedPreferences.getAll();

        for(Map.Entry<String,?> entry : keys.entrySet()){
            if(!entry.getValue().equals(999)){
                editor.remove(entry.getKey());
            }
        }

        //Commit
        editor.commit();
    }
}
