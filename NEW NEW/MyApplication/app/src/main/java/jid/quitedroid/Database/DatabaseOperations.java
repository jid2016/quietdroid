package jid.quitedroid.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by JiYeon on 2016-06-28.
 */
//Main Database Operations
public class DatabaseOperations extends SQLiteOpenHelper {
    private static final int database_version = 1;

    private static final int
            EVENT_TITLE = 0,
            EVENT_START_TIME = 1,
            EVENT_END_TIME = 2,
            EVENT_MODE = 3,
            GROUP_ID = 4,
            EVENT_ID = 5,
            EVENT_RRULE = 6;

    private static final int
            CALLER_NAME = 0,
            CALLER_EVENT_TITLE = 1,
            CALLER_MODE = 2,
            CALLER_TIME = 3;

    private long endTime = 0;

    public DatabaseOperations(Context context) {
        super(context, TableData.TableInfo.DATABASE_NAME, null, database_version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Create four table for QuiteDroid database
        db.execSQL(TableData.TableInfo.CREATE_QUERY_RAW);
        db.execSQL(TableData.TableInfo.CREATE_QUERY_ORGANIZED);
        db.execSQL(TableData.TableInfo.CREATE_QUERY_FORCED);
        db.execSQL(TableData.TableInfo.CREATE_QUERY_CALL_LOG);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    //Check if a specific event is in the raw table of database
    public Boolean isEventInExtractedTable(String eventTitle, String startTime, String endTime, boolean isRecurringEvent){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase(); //Initialize database

        //if recurring event, do not check dtend, as it will be null
        String selection = isRecurringEvent?
                TableData.TableInfo.EVENT_TITLE + "=? AND " + TableData.TableInfo.EVENT_START_TIME + "=?" :
                TableData.TableInfo.EVENT_TITLE + "=? AND " + TableData.TableInfo.EVENT_START_TIME + "=? AND " + TableData.TableInfo.EVENT_END_TIME + " =?";

        String[] arguments = isRecurringEvent?
                new String[]{eventTitle, startTime} :
                new String[]{eventTitle, startTime, endTime};

        Cursor cursor = sqLiteDatabase.query(TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS, new String[]{TableData.TableInfo.EVENT_TITLE, TableData.TableInfo.EVENT_START_TIME, TableData.TableInfo.EVENT_END_TIME},
                selection, arguments,
                null, null, null); //Query database, find all events with the same event INSTANCES_ID

        if(cursor != null && cursor.getCount() > 0)
        {
            return true;
        }else
        {
            return false;
        }
    }

    //Search for the event in Forced DB using event title and event INSTANCES_ID
    //If match found, return the mode of the event
    //If match has not been found, return -1
    public int isEventInForcedDb(String eventTitle, String eventID){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase(); //Initialize database
        //Check event title and event id
        String selection = TableData.TableInfo.EVENT_TITLE + "=? AND " + TableData.TableInfo.EVENT_ID + "=?";

        String[] arguments = new String[]{eventTitle, eventID};

        Cursor cursor = sqLiteDatabase.query(TableData.TableInfo.TABLE_NAME_FORCED, new String[]{TableData.TableInfo.EVENT_MODE},
                selection, arguments,
                null, null, null); //Query database, find all events with the same event INSTANCES_ID

        showInformation(TableData.TableInfo.TABLE_NAME_FORCED);

        cursor.moveToFirst();

        if(cursor != null && cursor.getCount() > 0)
        {
            int forced_mode = 0;
            return cursor.getInt(forced_mode);
        }else
        {
            return -1;
        }
    }

    //Insert event into database (RAW/ORGANIZED)
    public void insert(DatabaseOperations databaseOperations, String tableName, String titleOfEvent, long dtStart, long dtEnd, int mode, int groupID, String eventID, String RRULE) {
        SQLiteDatabase sqLiteDatabase = databaseOperations.getWritableDatabase(); //enable write to database

        //Content values to insert to database
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableData.TableInfo.EVENT_TITLE, titleOfEvent);
        try {
            contentValues.put(TableData.TableInfo.EVENT_START_TIME, dtStart);
            contentValues.put(TableData.TableInfo.EVENT_END_TIME, dtEnd);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        contentValues.put(TableData.TableInfo.EVENT_MODE, mode);
        contentValues.put(TableData.TableInfo.GROUP_ID, groupID);
        contentValues.put(TableData.TableInfo.EVENT_ID, eventID);
        contentValues.put(TableData.TableInfo.EVENT_RRULE, RRULE);

        //Insert content values to database
        sqLiteDatabase.insert(tableName, null, contentValues);
    }

    //Transfer an event from the raw table to the forced table
    public void transferEventFromRawToForced(String eventTitle, String eventID){
        Cursor cursor = getForcedEventCursor(eventID);
        if(cursor == null) {
            return;
        }

        cursor.moveToFirst();

        //Delete the event in FORCED db if it already exists
        //Check for the event using event INSTANCES_ID and event Title
        this.deleteEventData(TableData.TableInfo.TABLE_NAME_FORCED, eventTitle, eventID);

        this.insert(this,
                TableData.TableInfo.TABLE_NAME_FORCED, cursor.getString(EVENT_TITLE),
                cursor.getLong(EVENT_START_TIME),
                cursor.getLong(EVENT_END_TIME),
                cursor.getInt(EVENT_MODE),
                cursor.getInt(GROUP_ID),
                cursor.getString(EVENT_ID),
                cursor.getString(EVENT_RRULE));

        cursor.close();
    }

    //Insert the rejected caller's details into the call log table
    public void insertCallLog(DatabaseOperations databaseOperations, String callerName, String eventTitle, String mode, String currentTime){
        SQLiteDatabase sqLiteDatabase = databaseOperations.getWritableDatabase(); //enable write to database
        //Content values to insert to database
        ContentValues contentValues = new ContentValues();
        try {
            contentValues.put(TableData.TableInfo.CALLER_NAME, callerName);
            contentValues.put(TableData.TableInfo.CALLER_EVENT_TITLE, eventTitle);
            contentValues.put(TableData.TableInfo.CALLER_MODE, mode);
            contentValues.put(TableData.TableInfo.CALLER_TIME, currentTime);
        }catch(Exception ex){
            ex.printStackTrace();
        }

        //Insert content values to database
        sqLiteDatabase.insert(TableData.TableInfo.TABLE_NAME_BLOCKED_CALL_LOG, null, contentValues);
    }

    //Return a cursor object (RAW/ORGANIZED)
    public Cursor getCursor(DatabaseOperations databaseOperations, String tableName) {
        //Read from the database
        SQLiteDatabase sqLiteDatabase = databaseOperations.getReadableDatabase();
        //Store column names in a string array
        String[] columns = {TableData.TableInfo.EVENT_TITLE, TableData.TableInfo.EVENT_START_TIME, TableData.TableInfo.EVENT_END_TIME, TableData.TableInfo.EVENT_MODE, TableData.TableInfo.GROUP_ID, TableData.TableInfo.EVENT_ID, TableData.TableInfo.EVENT_RRULE};
        //Request query
        Cursor cursor = sqLiteDatabase.query(tableName, columns, null, null, null, null, null);
        return cursor;
    }

    //Return a cursor object (CALL LOG)
    public Cursor getCallLogCursor(DatabaseOperations databaseOperations){
        //Read from the database
        SQLiteDatabase sqLiteDatabase = databaseOperations.getReadableDatabase();
        //Store column names in a string array
        String[] columns = {TableData.TableInfo.CALLER_NAME, TableData.TableInfo.CALLER_EVENT_TITLE, TableData.TableInfo.CALLER_MODE, TableData.TableInfo.CALLER_TIME};
        //Request query
        Cursor cursor = sqLiteDatabase.query(TableData.TableInfo.TABLE_NAME_BLOCKED_CALL_LOG, columns, null, null, null, null, null);
        return cursor;
    }

    //Return a cursor object corresponding to the input groupID
    public Cursor getNewCursor(DatabaseOperations databaseOperations, String groupID){
        //Read from the database
        SQLiteDatabase sqLiteDatabase = databaseOperations.getReadableDatabase();
        //Store column names in a string array
        String[] columns = {TableData.TableInfo.EVENT_TITLE, TableData.TableInfo.EVENT_START_TIME, TableData.TableInfo.EVENT_END_TIME, TableData.TableInfo.EVENT_MODE, TableData.TableInfo.GROUP_ID, TableData.TableInfo.EVENT_ID, TableData.TableInfo.EVENT_RRULE};
        //Request query
        Cursor cursor = sqLiteDatabase.query(TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS, columns, TableData.TableInfo.GROUP_ID + " LIKE ?", new String[]{"%" + groupID + "%"}, null, null, null, null);
        return cursor;
    }

    //Return a cursor object (FORCED)
    public Cursor getForcedEventCursor(String eventID){
        //Read from the database
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        //Store column names in a string array
        String[] columns = {TableData.TableInfo.EVENT_TITLE, TableData.TableInfo.EVENT_START_TIME, TableData.TableInfo.EVENT_END_TIME, TableData.TableInfo.EVENT_MODE, TableData.TableInfo.GROUP_ID, TableData.TableInfo.EVENT_ID, TableData.TableInfo.EVENT_RRULE};
        //Request query
        Cursor cursor = sqLiteDatabase.query(TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS, columns, TableData.TableInfo.EVENT_ID + " LIKE ?", new String[]{eventID}, null, null, null, null);
        return cursor;
    }

    //Get the title of all the events in the RAW table
    public List<String> getAllEventTitles(){
        //Set new list
        List<String> list = new ArrayList<String>();
        //Create cursor
        Cursor cursor = getCursor(this, TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS);
        //Move cursor to first row
        cursor.moveToFirst();

        //Continue while there is a next row to move to
        do {
            try{
                list.add(cursor.getString(EVENT_TITLE));
            }catch(Exception ex){
                ex.printStackTrace();
            }
        } while (cursor.moveToNext());

        //Close cursor
        cursor.close();

        return list;
    }


    //Get all the event's times in the RAW table
    public List<String> getAllEventTimes(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("E  hh:mmaa");
        Calendar calendar;
        String formattedDate;

        //Set new list
        List<String> list = new ArrayList<String>();
        //Create cursor
        Cursor cursor = getCursor(this, TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS);
        //Move cursor to first row
        cursor.moveToFirst();

        //Continue while there is a next row to move to
        do {
            try{
                calendar = Calendar.getInstance();
                calendar.setTimeInMillis(cursor.getLong(EVENT_START_TIME));
                formattedDate = dateFormat.format(calendar.getTime());
                list.add(formattedDate);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        } while (cursor.moveToNext());

        //Close cursor
        cursor.close();

        return list;
    }

    //Return a list of group INSTANCES_ID
    public ArrayList<String> getGroupIDList(){
        ArrayList<String> groupIDList = new ArrayList<>();
        //Create cursor
        Cursor cursor = getCursor(this, TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS);
        //Move cursor to first row
        cursor.moveToFirst();
        //Initialize string to compare with
        String compareGroupID = "";

        //Continue while there is a next row to move to
        do {
            try{
                //Compare the last element with the current group INSTANCES_ID in db, if they are not the same insert into list
                if(!compareGroupID.equals(cursor.getString(GROUP_ID))){
                    groupIDList.add(cursor.getString(GROUP_ID));
                    compareGroupID = groupIDList.get(groupIDList.size() - 1);
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }
        } while (cursor.moveToNext());

        //Close cursor
        cursor.close();

        return groupIDList;
    }

    //Set new mode for a specific event
    public void setNewEventToggleMode(String eventID, int mode){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase(); //enable write to database
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableData.TableInfo.EVENT_MODE, mode);
        //Change mode
        sqLiteDatabase.update(TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS, contentValues, TableData.TableInfo.EVENT_ID + " LIKE ?", new String[]{eventID});
    }

    //Set new event INSTANCES_ID for a specific event
    public String setNewEventID(DatabaseOperations databaseOperations, String eventID){
        int numberOfSameEvents = 0; //Initialize number of same events
        Boolean start = false; //Set start flag as false
        SQLiteDatabase sqLiteDatabase = databaseOperations.getWritableDatabase(); //Initialize database
        Cursor cursor = sqLiteDatabase.query(TableData.TableInfo.TABLE_NAME_SCHEDULED_EVENTS, new String[]{TableData.TableInfo.EVENT_ID},
                TableData.TableInfo.EVENT_ID + " LIKE ?", new String[]{"%" + eventID + "%"},
                null, null, null); //Query database, find all events with the same event INSTANCES_ID
        cursor.moveToFirst();

        if(cursor.moveToFirst()){ //Set cursor to first event
            start = true;
        }

        //Iterate through database and find number of events with same eventID
        do{
            if(start) numberOfSameEvents++; //Increment number of same events
        }while(cursor.moveToNext());
        //Close cursor
        cursor.close();
        //Return new event INSTANCES_ID
        return buildNewEventID(eventID, numberOfSameEvents);
    }

    //Build new event INSTANCES_ID for the organized table
    private String buildNewEventID(String eventID, int numberOfEvents){
        StringBuilder sb = new StringBuilder();
        sb.append(eventID);
        sb.append("_");
        sb.append(numberOfEvents);
        return sb.toString();
    }

    //Find the latest end time in the organized table
    public long findLatestTime(DatabaseOperations databaseOperations) {
        try {
            //Create cursor
            Cursor cursor = getCursor(this, TableData.TableInfo.TABLE_NAME_SCHEDULED_EVENTS);
            //Move cursor to last row
            cursor.moveToLast();
            //Store most recent time
            endTime = cursor.getLong(EVENT_END_TIME);
            //Close cursor
            cursor.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return endTime;
    }

    //Set end time as the earliest time found in the raw database
    public void setEarliestTime(long newTime) {
        endTime = newTime;
    }

    //Delete all data from Table
    public void deleteTableData(DatabaseOperations databaseOperations, String tableName) {
        //enable write to database
        SQLiteDatabase sqLiteDatabase = databaseOperations.getWritableDatabase();
        sqLiteDatabase.delete(tableName, null, null);
    }

    /*Used on weekly trigger (delete non-forced events from forced db) */
    public void cleanForcedDb(){
        //enable write to database
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String whereClause = TableData.TableInfo.EVENT_RRULE + " LIKE ?";
        sqLiteDatabase.delete(TableData.TableInfo.TABLE_NAME_FORCED, whereClause, new String[]{"Normal Event"});
    }

    /*Used on quick add handler, when reverted delete event from forced db */
    public void deleteEventInForcedDb(String eventTitle, String eventID){
        //enable write to database
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String whereClause = TableData.TableInfo.EVENT_TITLE + "=? AND " + TableData.TableInfo.EVENT_ID + "=?";
        sqLiteDatabase.delete(TableData.TableInfo.TABLE_NAME_FORCED, whereClause, new String[]{eventTitle, eventID});
    }

    //Delete a particular row (event) in database if event title and event id matches
    public void deleteEventData(String tableName, String eventTitle, String eventID){
        //enable write to database
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        try{
            String query = TableData.TableInfo.EVENT_TITLE + "=? AND " + TableData.TableInfo.EVENT_ID + "=?";
            sqLiteDatabase.delete(tableName, query, new String[] { eventTitle, eventID });
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    //Log the content of database (RAW/ORGANIZED)
    public void showInformation(String tableName){
        //Create cursor
        Cursor cursor = getCursor(this, tableName);
        //Move cursor to first row
        cursor.moveToFirst();
        //Continue while there is a next row to move to
        do{
            try{
<<<<<<< HEAD
//                Log.d("In the Database", cursor.getString(EVENT_TITLE) + " START t: " + cursor.getString(EVENT_START_TIME) + " INSTANCES_END t: " + cursor.getString(EVENT_END_TIME) + " MODE: " + cursor.getString(EVENT_MODE) + " GroupID: " + cursor.getString(GROUP_ID) + " UniqueID: " + cursor.getString(INSTANCES_EVENT_ID) + "KEY" + cursor.getString(EVENT_RRULE));
=======
                Log.d("In the Database", cursor.getString(EVENT_TITLE) + " START t: " + cursor.getString(EVENT_START_TIME) + " END t: " + cursor.getString(EVENT_END_TIME) + " MODE: " + cursor.getString(EVENT_MODE) + " GroupID: " + cursor.getString(GROUP_ID) + " UniqueID: " + cursor.getString(EVENT_ID) + "KEY" + cursor.getString(EVENT_RRULE));
>>>>>>> feature/Priority_Caller_Feature
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }while(cursor.moveToNext());
    }

    //Log the content of call log database
    public String createCallLog(){
        //Create string builder
        StringBuilder stringBuilder = new StringBuilder();
        //Create cursor
        Cursor cursor = getCallLogCursor(this);
        //Move cursor to first row
        cursor.moveToLast();

        do{
            try{
                stringBuilder.append("Caller: " + cursor.getString(CALLER_NAME));
                stringBuilder.append(System.getProperty("line.separator"));
                stringBuilder.append("Event Title: " + cursor.getString(CALLER_EVENT_TITLE));
                stringBuilder.append(System.getProperty("line.separator"));
                stringBuilder.append("Time: " + cursor.getString(CALLER_TIME));
                stringBuilder.append(System.getProperty("line.separator"));
                stringBuilder.append(System.getProperty("line.separator"));
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }while(cursor.moveToPrevious());

        return stringBuilder.toString();
    }

    //Get all event INSTANCES_ID
    public List<String> getAllEventIds(String tableName) {
        List<String> eventIds = new ArrayList<>();
        //Create cursor
        Cursor cursor = getCursor(this, tableName);
        //Move cursor to first row
        cursor.moveToFirst();

        //Continue while there is a next row to move to
        do {
            try{
                eventIds.add(cursor.getString(EVENT_ID));
            }catch(Exception ex){
                ex.printStackTrace();
            }
        } while (cursor.moveToNext());

        //Close cursor
        cursor.close();
        return eventIds;
    }

    //Get all event modes
    public List<String> getAllEventModes(String tableName) {
        List<String> eventIds = new ArrayList<>();
        //Create cursor
        Cursor cursor = getCursor(this, tableName);
        //Move cursor to first row
        cursor.moveToFirst();

        //Continue while there is a next row to move to
        do {
            try{
                eventIds.add(cursor.getString(EVENT_MODE));
            }catch(Exception ex){
                ex.printStackTrace();
            }
        } while (cursor.moveToNext());

        //Close cursor
        cursor.close();
        return eventIds;
    }

    //Get all event RRULE
    public List<String> getAllEventRRULE(){
        List<String> eventRRULE = new ArrayList<>();
        //Create cursor
        Cursor cursor = getCursor(this, TableData.TableInfo.TABLE_NAME_EXTRACTED_EVENTS);
        //Move cursor to first row
        cursor.moveToFirst();

        //Continue while there is a next row to move to
        do {
            try{
                eventRRULE.add(cursor.getString(EVENT_RRULE));
            }catch(Exception ex){
                ex.printStackTrace();
            }
        } while (cursor.moveToNext());

        //Close cursor
        cursor.close();
        return eventRRULE;
    }
}
