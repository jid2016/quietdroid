package jid.quitedroid.Handlers;

//From the ITelephony.aidl file

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import jid.quitedroid.Database.DatabaseOperations;
import jid.quitedroid.Features.PriorityCallHandler;
import jid.quitedroid.GlobalConstants;
import jid.quitedroid.Modes.SilentMode;
import jid.quitedroid.R;

/**
 * Created by JiYeon on 2016-03-20.
 */

public class callHandler extends BroadcastReceiver {
    private static String TAG = "PhoneStateReceiver";
    private static final String MyPREFERENCES = "ExceptionList";
    private PriorityCallHandler priorityCallHandler = new PriorityCallHandler(ContextHandler.getContext()); //Priority Call Handler
    private SoundHandler soundHandler = new SoundHandler(); //Sound Handler
    private messageHandler msgHandler = new messageHandler(ContextHandler.getContext()); //Message Handler
    private DatabaseOperations databaseOperations = new DatabaseOperations(ContextHandler.getContext());//Database Operations
    private ModeHandler modeHandler = new ModeHandler(); //Mode Handler
    private SharedPreferences sharedQuiteDroidEnabled;

    @Override
    public void onReceive(Context context, Intent intent) {
        //Exit if quite droid is not enabled
        sharedQuiteDroidEnabled = PreferenceManager.getDefaultSharedPreferences(context);
        boolean quiteDroid_isEnabled = sharedQuiteDroidEnabled.getBoolean(GlobalConstants.QUITEDROID_ENABLED, false);
        if(!quiteDroid_isEnabled){
            return;
        }

        Bundle bundle = intent.getExtras();
        String state = bundle.getString(TelephonyManager.EXTRA_STATE);

        //If the OnReceive activity was not called by the incoming call, return. No need to continue.
        if (!TelephonyManager.EXTRA_STATE_RINGING.equalsIgnoreCase(state)) {
            return;
        }

        //When the state of the phone changes, deny all incoming calls
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        try {
            Class c = Class.forName(tm.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            ITelephony telephonyService = (ITelephony) m.invoke(tm);
            String phoneNumber = bundle.getString("incoming_number");
            String caller;

            if (blockCall(context, phoneNumber)) {
                telephonyService.endCall();

                //If caller is not listed in the user's contacts list, show caller's name as New Caller in the blocked call list
                caller = (getContactName(context, phoneNumber) == null) ? "New Caller" : getContactName(context, phoneNumber);

                //Show as toast test: Rejected Phone Call From 'caller's name'
                Toast.makeText(context, "Rejected Phone Call From:" + caller, Toast.LENGTH_LONG).show();

                //Enter data into blocked call db
                databaseOperations.insertCallLog(databaseOperations, caller, modeHandler.getEventTitle(), modeHandler.getMode(), getCurrentDateTime());

                //Record caller for the priority caller feature
                priorityCallHandler.recordCaller(phoneNumber);

                //show blocked call notification (separate notification)
                showBlockedCallNotification(context);

                //Generate rejection message according to mode
                msgHandler.sendSMSMessage(phoneNumber, modeHandler.getMode());

                //Update the Main UI
                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
                localBroadcastManager.sendBroadcast(new Intent(GlobalConstants.FILTER_MAINUI_REFRESH));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Determine if the incoming call should be passed through or not
    private boolean blockCall(Context context, String phoneNumber) {
        String current_mode = modeHandler.getMode();

        //If current mode is Blocking Mode, return false
        if (current_mode.equals(SilentMode.name)) {
            if (isCallerInExceptionList(context, phoneNumber)) {
                //If the caller is in the exception list in blocking mode, do not deny the call
                return false;
            }else{
                //If the caller is NOT in the exception list in blocking mode, deny call and send rejection message
                return true;
            }

        }

        //All other cases, pass through call
        return false;
    }

    //Check if the caller is listed on the exception list
    //If caller is in the exception list, return true. Otherwise return false
    private boolean isCallerInExceptionList(Context context, String phoneNumber) {
        //Retrieve exception list from shared preferences
        SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        Map<String, ?> keys = sharedpreferences.getAll();

        //Iterate through exception list
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            //If caller's phone number matches, return TRUE
            if (entry.getValue().toString().trim().equals(phoneNumber.trim())) {
                return true;
            }
        }
        //Caller is not listed in the exception list, return false
        return false;
    }

    //Get current date and time
    private String getCurrentDateTime() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time =&gt; " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd/mm/yyyy E  hh:mmaa");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    //Find the contact's name using their phone number
    private static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

        if (cursor == null) {
            return "Priority Caller";
        }

        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return contactName;
    }

    //When QuiteDroid has rejected a call, show a blocked call notification
    public void showBlockedCallNotification(Context context) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.blockedcallicon)
                        .setContentTitle("Quite Droid")
                        .setAutoCancel(true)
                        .setContentText("Calls have been rejected...click to view call log");

        //Intent for missed call log
        Intent resultIntent = new Intent();
        resultIntent.setAction(Intent.ACTION_VIEW);
        resultIntent.setType(CallLog.Calls.CONTENT_TYPE);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);

        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(GlobalConstants.BLOCKED_CALL_NOTIFICATION_ID, mBuilder.build());

    }
}
